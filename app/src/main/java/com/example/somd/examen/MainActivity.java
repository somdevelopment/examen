package com.example.somd.examen;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    FragmentPagerAdapter adaptadorPaginas;


    private boolean isNetDisponible() {

        ConnectivityManager connectivityManager = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo actNetInfo = connectivityManager.getActiveNetworkInfo();



        return (actNetInfo != null && actNetInfo.isConnected());
    }

    private boolean revisaPermisoInternet() {
        int tienePermiso = ContextCompat.checkSelfPermission(this,
                Manifest.permission.INTERNET);
        if(tienePermiso != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[] { Manifest.permission.INTERNET },
                    0);
            return false;
        }
        return true;
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if(revisaPermisoInternet()){
            if(isNetDisponible()){

                    ViewPager paginador = (ViewPager) findViewById(R.id.paginador);
                    adaptadorPaginas = new AdaptadorPaginas(getSupportFragmentManager());
                    if(paginador != null){
                        paginador.setAdapter(adaptadorPaginas);

                    }


            }else {
                Toast.makeText(getApplicationContext(),"Sin accedo a internet",Toast.LENGTH_LONG).show();
            }
        }else {
            Toast.makeText(getApplicationContext(),"Sin permisos de internet",Toast.LENGTH_LONG).show();
        }


    }

    public static class AdaptadorPaginas extends FragmentPagerAdapter {
        public static int NUM_ITEMS = 2;

        public AdaptadorPaginas(FragmentManager fm) {
            super(fm);
        }

        public Fragment getItem(int position) {
            switch (position) {

                case 0:
                    return AudioFragment.newInstance();
                case 1:
                    return VideoFragment.newInstance();
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return NUM_ITEMS;
        }

        public CharSequence getPageTitle(int i) {
            switch (i) {
                case 0:
                    return "Audio";
                case 1:
                    return "Video";
                default:
                    return "";
            }
        }
    }
}
