package com.example.somd.examen;
import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.concurrent.ExecutionException;




public class VideoFragment extends Fragment {
    ListView lista;
    ImageButton reproducir, parar, rebobinar, avanzar;
    VideoView video;
    String ruta;
    ArrayList<String> videoList;
    final String URL_VIDEO_MV = "https://www.mercadoverde.com.mx/pruebasDaniel/android/getVideos.php";
    final String RUTA_VIDEO = "https://www.mercadoverde.com.mx/pruebasDaniel/android/videos/";
    int currente = 0;

    TextView mensaje, autor;
    MediaPlayer mp;
    TextView duracion;

    SeekBar mediaSeekBarTime = null;
    SeekBar mediaSeekBarVolumen = null;
    AudioManager audioManager;

    public VideoFragment() {
        // Required empty public constructor
    }

    public static VideoFragment newInstance() {
        VideoFragment fragment = new VideoFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public void iniciar(String nombre) throws IOException {
        String[] arrStr = nombre.split("-");
        nombre = nombre.replace(" ", "%20");
        // url = url del webservices más el nombre de archivo del video
        String url=RUTA_VIDEO +nombre+".mp4";

        Toast.makeText(getContext(),"Abriendo:\n"+url,Toast.LENGTH_LONG).show();
        video.setVideoPath(url);
        String titulo = arrStr[1];
        String artista = arrStr[0];


        autor.setText("Autor: " + artista);
        mensaje.setText(titulo);


        currente = 0;
        video.start();
        reproducir.setImageResource(R.mipmap.pausar);
        video.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {

                mediaSeekBarTime.setMax(video.getDuration());
                mediaSeekBarTime.postDelayed(onEverySecond, 1000);

            }
        });
        // actualizamos seekBar para que siga el progremso de la cancion
            mediaSeekBarTime.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if(fromUser){
                    if(fromUser) {
                        video.seekTo(progress);
                        duracion.setText(String.format("%1$tH:%1$tM:%1$tS", new Date(seekBar.getMax())));
                    }

                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });





    }
    private Runnable onEverySecond=new Runnable() {

        @Override
        public void run() {

            if(mediaSeekBarTime != null) {
                mediaSeekBarTime.setProgress(video.getCurrentPosition());
            }

            if(video.isPlaying()) {
                mediaSeekBarTime.postDelayed(onEverySecond, 1000);
                duracion.setText(String.format("%1$tM:%1$tS", new Date(mediaSeekBarTime.getProgress())) + "/" + String.format("%1$tM:%1$tS", new Date(mediaSeekBarTime.getMax())));
            }

        }
    };
    // Destruimos mediaPlayer al termino de la cancion
    public void destruir() {
        if(mp != null) {
            mp.release();
        }
    }


    public ArrayList<String> obtenListadoVideos(){
        // -. OBTENER S LISTADO DE VIDEOS ---
        JSONObject json = null;
        videoList = new ArrayList<String>();
        String gg = null;

        try {
            // -. OBTENEMOS LISTADO DE VIDEOS DEL SERVIDOR MEDIANTE UN WEB SERVICES ---
            gg = new VideoFragment.Video().execute().get();

            if(gg.equals("")){
                return videoList;
            }

            json = new JSONObject(gg);

            Iterator<String> temp = json.keys();
            while (temp.hasNext()) {
                String key = temp.next();
                Object value = null;
                try {
                    value = json.get(key);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getContext(), "Problemas al conectar al servidor", Toast.LENGTH_LONG).show();
                }
                // CADA CANCION LA AGREGAMOS A LA LISTA DE CANCIONES
                if (!value.equals("valido")) {
                    value = value.toString().replaceFirst("[\\s\\S]{0,4}$", "");
                    videoList.add(value.toString());
                }
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
            Toast.makeText(getContext(),"Problemas al conectar al servidor", Toast.LENGTH_LONG).show();
        } catch (ExecutionException e) {
            e.printStackTrace();
            Toast.makeText(getContext(),"Problemas al conectar al servidor", Toast.LENGTH_LONG).show();
        } catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(getContext(),"Problemas al conectar al servidor", Toast.LENGTH_LONG).show();
        }

        return videoList;
        // -------------------------------------------------------------------------
    }





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View vista = inflater.inflate(R.layout.fragment_video, container, false);
        mensaje = (TextView) vista.findViewById(R.id.mensaje);
        autor = (TextView) vista.findViewById(R.id.videoAutor);
        lista = (ListView) vista.findViewById(R.id.videoList);
        video = (VideoView) vista.findViewById(R.id.video);
        reproducir = (ImageButton) vista.findViewById(R.id.reproducir_pausar);
        parar = (ImageButton) vista.findViewById(R.id.parar);
        rebobinar = (ImageButton) vista.findViewById(R.id.rebobinar);
        avanzar = (ImageButton) vista.findViewById(R.id.avanzar);
        mediaSeekBarTime = (SeekBar) vista.findViewById(R.id.seekBarVideo);
        duracion = (TextView) vista.findViewById(R.id.lbl_duration);

        //ruta = "android.resource://com.example.somd.examen/"+R.raw.patineta;
        ruta ="https://www.mercadoverde.com.mx/pruebasDaniel/android/videos/Eddie_Loaeza_Akuphel.mp4";
        //video.setVideoPath(ruta);
        //-- inicializamos SeekBar de control de volumen -----------------------------------------
        mediaSeekBarVolumen = (SeekBar) vista.findViewById(R.id.seekBarVolumen);
        audioManager = (AudioManager) getActivity().getSystemService(Context.AUDIO_SERVICE);
        mediaSeekBarVolumen.setMax(audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC));
        mediaSeekBarVolumen.setProgress(audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC));
        mediaSeekBarVolumen.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, progress, 0);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        //------------------------------------------------------------------------------------------

        videoList = obtenListadoVideos();

        if (videoList.size() == 0){
            mensaje.setText("No hay video para reproducir");
        }

        ArrayAdapter<String> videoAdapter  = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1,videoList);
        lista.setAdapter(videoAdapter);

        // Se selleciona una cancion
        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                destruir();
                // obtenemos que cancio fue seleccionada
                String nombre = videoList.get(position);
                try {
                    // iniciamos reproduccion
                    iniciar(nombre);
                    reproducir.setVisibility(View.VISIBLE);
                    parar.setVisibility(View.VISIBLE);
                    //mensaje.setText(nombre);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        //--------------------------------------------------------------------------------------------------------------------------------------




        reproducir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!video.isPlaying()) {
                    reproducir.setImageResource(R.mipmap.pausar);
                    video.start();
                    mediaSeekBarTime.postDelayed(onEverySecond, 1000);
                } else {
                    reproducir.setImageResource(R.mipmap.reproducir);
                    video.pause();

                }
            }
        });

        parar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(video.isPlaying()) {
                    reproducir.setImageResource(R.mipmap.reproducir);
                    video.stopPlayback();
                    video.resume();
                    mensaje.setText("Sin reproduccir");
                    duracion.setText("00:00/00:00");
                    autor.setText("");

                }
            }
        });

        rebobinar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(video.isPlaying() && video.canSeekBackward()) {
                    video.seekTo(video.getCurrentPosition() - 1000);
                }
            }
        });

        avanzar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(video.isPlaying() && video.canSeekForward()) {
                    video.seekTo(video.getCurrentPosition() + 1000);
                }
            }
        });

        // Inflate the layout for this fragment
        return vista;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        try
        {
            if (!isVisibleToUser && video != null) {
                if(!video.isPlaying()) {
                    reproducir.setImageResource(R.mipmap.pausar);
                    video.start();
                    mediaSeekBarTime.postDelayed(onEverySecond, 1000);
                } else {
                    Toast.makeText(getContext(),"Pausado.",Toast.LENGTH_SHORT).show();
                    reproducir.setImageResource(R.mipmap.reproducir);
                    video.pause();

                }


            }
        }
        catch(NullPointerException e)
        {
            e.printStackTrace();
            Toast.makeText(getContext(),"Ocurrio un problena inesperado.",Toast.LENGTH_LONG).show();
        }

    }

    public class Video extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {
            String respuesta = "";

            try {
                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("usuario", "somd")
                        .appendQueryParameter("password", "1234567");

                String parametros = builder.build().getEncodedQuery();
                // se establece la url peticionURLmusic a conectar
                respuesta = obtenMusica(URL_VIDEO_MV, parametros);

            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(getContext(),"Problemas al conectar con el servidor", Toast.LENGTH_LONG).show();
                return respuesta;
            }
            return respuesta;
        }

        private String obtenMusica(String path, String params) throws IOException {
            StringBuilder sb = null;
            try {
                URL url = new URL(path);// Aqui la rita del web Services
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("POST");
                conn.setDoOutput(true);
                OutputStream out = conn.getOutputStream();
                out.write(params.getBytes());
                out.flush();
                out.close();
                int responseCode = conn.getResponseCode();
                if(responseCode == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(conn.getInputStream()));
                    String linea;
                    sb = new StringBuilder();
                    while ( (linea = br.readLine()) != null) {
                        sb.append(linea);
                    }
                    br.close();


                }
                //devolvemos un String con las canciones
                if(sb != null) {
                    return sb.toString();
                }else {
                    return "";
                }
            }catch (Exception e){
                Toast.makeText(getContext(),"Problemas al conectar con el servidor.", Toast.LENGTH_LONG).show();

            }
            return sb.toString();

        }

    }


}