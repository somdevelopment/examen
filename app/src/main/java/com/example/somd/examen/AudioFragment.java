package com.example.somd.examen;


import android.content.Context;
import android.media.AudioManager;
import android.media.MediaMetadataRetriever;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.ExecutionException;

public class AudioFragment extends Fragment {


    // urls de peticion, web services
    final String peticionURLmusic = "https://www.mercadoverde.com.mx/pruebasDaniel/android/getCanciones.php";
    final String URL_MUSIC_MV = "https://www.mercadoverde.com.mx/pruebasDaniel/android/music/";
    //-----------------------------------------------------------------------------------------------------------

    ListView lista;
    MediaPlayer mp;
    Button reproduce, pausa, detiene, rebobinar, avanzar;
    ArrayList<String> cancionesLst;

    Runnable runnable;

    SeekBar mediaSeekBar = null;
    SeekBar mediaSeekBarTime = null;
    AudioManager audioManager = null;

    TextView pista, duracion, autor;

    // contructor del fragment
    public AudioFragment() {
        // Required empty public constructor
    }

    // intancia del fragmen ---
    public static AudioFragment newInstance() {
        AudioFragment fragment = new AudioFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    // Inicializa la cancion, recibe como parametro en nombre de archivo
    public void iniciar(String nombre) throws IOException {

        mp=new MediaPlayer();
        nombre = nombre.replace(" ", "%20");

        // url = url del webservices más el nombre de archivo de la cancion
        String url=URL_MUSIC_MV+nombre;
        //Toast.makeText(getContext(),"Abriendo:\n"+url,Toast.LENGTH_LONG).show();
        mp.setDataSource(url);
        mp.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mp.prepare();
        mp.start();

        MediaMetadataRetriever mmr = new MediaMetadataRetriever();
        mmr.setDataSource(url, new HashMap<String, String>());

        String album = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUM);
        String titulo = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE);
        String artista = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST);
        final String duracion = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION);

        autor.setText("Autor: " + artista);
        pista.setText(titulo);


        // iniciamos seekBar con la duración de la cancion
        mp.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                mediaSeekBarTime.setMax(Integer.parseInt(duracion));
                playCicle();
                mp.start();
                //duracion.setText(String.format("%1$tM:%1$tS", new Date(mp.getDuration())));
            }
        });
        // actualizamos seekBar para que siga el progremso de la cancion
        mediaSeekBarTime.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if(fromUser){
                    mp.seekTo(progress);
                    //pista.setText(progress + "/" + seekBar.getMax());
                    //duracion.setText(String.format("%1$tH:%1$tM:%1$tS", new Date(seekBar.getMax())));

                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


    }


    // Destruimos mediaPlayer al termino de la cancion
    public void destruir() {
        if(mp != null) {
            mp.release();
        }
    }

    // matenemos actualizado el seekBar de duracion de la cancion
    public void  playCicle(){
        mediaSeekBarTime.setProgress(mp.getCurrentPosition());

        if (mp.isPlaying()){
            runnable = new Runnable() {
                @Override
                public void run() {
                    playCicle();
                }
            };
            mediaSeekBarTime.postDelayed(runnable, 1000);
            //String.format("%1$tM:%1$tS", new Date(mp.getDuration()))
            //pista.setText(mediaSeekBarTime.getProgress() + "/" + mediaSeekBarTime.getMax());
            duracion.setText(String.format("%1$tM:%1$tS", new Date(mediaSeekBarTime.getProgress())) + "/" + String.format("%1$tM:%1$tS", new Date(mediaSeekBarTime.getMax())));
        }
    }

    public ArrayList<String> obtenListadoCanciones(){
        // -. OBTENER S LISTADO DE CANCIONES ---
        JSONObject json = null;
        cancionesLst = new ArrayList<String>();
        String gg = null;

        try {
            // -. OBTENEMOS LISTADO DE CANCIONES DEL SERVIDOR MEDIANTE UN WEB SERVICES ---
            gg = new Musica().execute().get();

            if(gg.equals("")){
                return cancionesLst;
            }

            json = new JSONObject(gg);

            Iterator<String> temp = json.keys();
            while (temp.hasNext()) {
                String key = temp.next();
                Object value = null;
                try {
                    value = json.get(key);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Toast.makeText(getContext(), "Problemas al conectar al servidor", Toast.LENGTH_LONG).show();
                }
                // CADA CANCION LA AGREGAMOS A LA LISTA DE CANCIONES
                if (!value.equals("valido")) {
                    cancionesLst.add(value.toString());
                }
            }

        } catch (InterruptedException e) {
            e.printStackTrace();
            Toast.makeText(getContext(),"Problemas al conectar al servidor", Toast.LENGTH_LONG).show();
        } catch (ExecutionException e) {
            e.printStackTrace();
            Toast.makeText(getContext(),"Problemas al conectar al servidor", Toast.LENGTH_LONG).show();
        } catch (JSONException e) {
            e.printStackTrace();
            Toast.makeText(getContext(),"Problemas al conectar al servidor", Toast.LENGTH_LONG).show();
        }

    return cancionesLst;
        // -------------------------------------------------------------------------
    }




    /// destruir media player al salir de la aplicacion
    @Override
    public void onDestroy() {
        super.onDestroy();
        mp.release();
        mediaSeekBarTime.removeCallbacks(runnable);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View vista = inflater.inflate(R.layout.fragment_audio, container, false);
        getActivity().setVolumeControlStream(AudioManager.STREAM_MUSIC);


                // -------------------------------------------------
        // recuperamos controladores de fragment_audio.xml
        lista = (ListView) vista.findViewById(R.id.lista);
        reproduce = (Button) vista.findViewById(R.id.reproduce_m);
        pausa = (Button) vista.findViewById(R.id.pausa_m);
        detiene = (Button) vista.findViewById(R.id.detiene_m);
        pista = (TextView) vista.findViewById(R.id.lbl_pista);
        duracion = (TextView) vista.findViewById(R.id.lbl_duration);
        autor = (TextView) vista.findViewById(R.id.lbl_autor);
        mediaSeekBar = (SeekBar) vista.findViewById(R.id.seekBar);
        mediaSeekBarTime = (SeekBar) vista.findViewById(R.id.seekBarTime);
        rebobinar = (Button) vista.findViewById(R.id.rebobinar);
        avanzar = (Button) vista.findViewById(R.id.avanzar);
        // -----------------------------------------------------------------


        //-- inicializamos SeekBar de control de volumen -----------------------------------------
        audioManager = (AudioManager) getActivity().getSystemService(Context.AUDIO_SERVICE);
        mediaSeekBar.setMax(audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC));
        mediaSeekBar.setProgress(audioManager.getStreamMaxVolume(AudioManager.STREAM_MUSIC));
        mediaSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, progress, 0);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        //------------------------------------------------------------------------------------------




        cancionesLst = obtenListadoCanciones();

        if(cancionesLst.size() == 0){
            pista.setText("Problemas al conectarse con el servidor. :(");
            duracion.setText("Sin canciones disponibles.");
        }

        // LLENAMOS  ListView con la lista de caciones
       ArrayAdapter<String> itemsAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, cancionesLst);
        lista.setAdapter(itemsAdapter);

             // Se selleciona una cancion
        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                destruir();
                // obtenemos que cancio fue seleccionada
                String nombre = cancionesLst.get(position);
                try {
                    // iniciamos reproduccion
                    Toast.makeText(getContext(),"Abriendo:\n"+URL_MUSIC_MV+nombre+".mp3",Toast.LENGTH_LONG).show();
                    iniciar(nombre);
                    pausa.setVisibility(View.VISIBLE);
                    detiene.setVisibility(View.VISIBLE);
                    rebobinar.setVisibility(View.VISIBLE);
                    avanzar.setVisibility(View.VISIBLE);
                    //pista.setText(nombre);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        //--------------------------------------------------------------------------------------------------------------------------------------

        reproduce.setVisibility(View.INVISIBLE);
        pausa.setVisibility(View.INVISIBLE);
        detiene.setVisibility(View.INVISIBLE);
        rebobinar.setVisibility(View.INVISIBLE);
        avanzar.setVisibility(View.INVISIBLE);

        reproduce.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mp != null && !mp.isPlaying()) {
                    mp.start();
                    reproduce.setVisibility(View.INVISIBLE);
                    pausa.setVisibility(View.VISIBLE);
                    playCicle();
                }
            }
        });

        detiene.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mp != null) {
                    mp.stop();
                    reproduce.setVisibility(View.INVISIBLE);
                    pausa.setVisibility(View.INVISIBLE);
                    detiene.setVisibility(View.INVISIBLE);
                    rebobinar.setVisibility(View.INVISIBLE);
                    avanzar.setVisibility(View.INVISIBLE);
                    pista.setText("Sin reproduccir");
                    duracion.setText("00:00/00:00");
                    autor.setText("");
                }
            }
        });

        pausa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mp!=null && mp.isPlaying()) {
                    mp.pause();
                    pausa.setVisibility(View.INVISIBLE);
                    reproduce.setVisibility(View.VISIBLE);
                }
            }
        });

        rebobinar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mp.isPlaying() ) {
                    mp.seekTo(mp.getCurrentPosition() - 1000);
                }
            }
        });

        avanzar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mp.isPlaying()) {
                    mp.seekTo(mp.getCurrentPosition() + 1000);
                }
            }
        });

        return vista;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        try
        {
            if(mp!=null && mp.isPlaying()) {
                mp.pause();
                pausa.setVisibility(View.INVISIBLE);
                reproduce.setVisibility(View.VISIBLE);
                Toast.makeText(getContext(),"Pausado.",Toast.LENGTH_SHORT).show();
            }
        }
        catch(NullPointerException e)
        {
            e.printStackTrace();
            Toast.makeText(getContext(),"Ocurrio un problena inesperado.",Toast.LENGTH_LONG).show();
        }

    }

    /// clase Musica nos ayuda a hacer la coneccion con el web servides
    public class Musica extends AsyncTask<String, String, String> {

        @Override
        protected String doInBackground(String... params) {
            String respuesta = "";

            try {
                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("usuario", "somd")
                        .appendQueryParameter("password", "1234567");

                String parametros = builder.build().getEncodedQuery();
                // se establece la url peticionURLmusic a conectar
                respuesta = obtenMusica(peticionURLmusic, parametros);

            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(getContext(),"Problemas al conectar con el servidor", Toast.LENGTH_LONG).show();
                return respuesta;
            }
            return respuesta;
        }

        private String obtenMusica(String path, String params) throws IOException {
            StringBuilder sb = null;
            try {
                URL url = new URL(path);// Aqui la rita del web Services
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setRequestMethod("POST");
                conn.setDoOutput(true);
                OutputStream out = conn.getOutputStream();
                out.write(params.getBytes());
                out.flush();
                out.close();
                int responseCode = conn.getResponseCode();
                if(responseCode == HttpURLConnection.HTTP_OK) {
                    BufferedReader br = new BufferedReader(
                            new InputStreamReader(conn.getInputStream()));
                    String linea;
                    sb = new StringBuilder();
                    while ( (linea = br.readLine()) != null) {
                        sb.append(linea);
                    }
                    br.close();


                }
                //devolvemos un String con las canciones
                if(sb != null) {
                    return sb.toString();
                }else {
                    return "";
                }
            }catch (Exception e){
                Toast.makeText(getContext(),"Problemas al conectar con el servidor.", Toast.LENGTH_LONG).show();

            }
            return sb.toString();

        }

    }

}

